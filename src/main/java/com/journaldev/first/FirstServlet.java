package com.journaldev.first;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
@WebServlet(description = "My First Servlet", urlPatterns = { "/FirstServlet" , "/FirstServlet.do"}, initParams = {@WebInitParam(name="id",value="1"),@WebInitParam(name="name",value="suraj")})
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String HTML_START="<html><body>";
	public static final String HTML_END="</body></html>";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Date date = new Date();
		StringBuffer requestURL = request.getRequestURL();
		String requestMethod = request.getMethod();
		Enumeration enumeration = request.getHeaderNames();
		String hostName = InetAddress.getLocalHost().getHostName()+" with IP="+InetAddress.getLocalHost().getHostAddress()+" ";
		out.println(HTML_START + "<h2>This Page is a result of push to git, auto build and deploy via jenkins for ansible</h2><br/>Date="+date +"<br>Host Details: ="+hostName +"<br>URL Details: ="+requestURL +"<br>Request Method: ="+requestMethod +""+HTML_END);
		while (enumeration.hasMoreElements()) {
		    String name=(String)enumeration.nextElement(); 
		    String value = request.getHeader(name);
		    out.println(HTML_START +"Name: "+name+" Value: "+value+"<br>"+HTML_END);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
